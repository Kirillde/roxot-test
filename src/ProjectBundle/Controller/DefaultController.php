<?php

namespace ProjectBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use ProjectBundle\Model\Processing;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     * @Template
     */
    public function indexAction(Request $request)
    {
        try{

            $model = $this->get('processing');
            $fileName = $model->generateHtml('football-master/source/matches/1024102.json');

            return [
                'status' => 'ok',
                'fileName' => $fileName,
                'dir' => $request->getBasePath()
            ];

        }catch(\Exception $e){
            return [
                'status' => $e->getMessage(),
            ];
        }
    }
}
