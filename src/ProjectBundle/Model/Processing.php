<?php

namespace ProjectBundle\Model;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Processing
{
    protected $templating;
    protected $data;

    public function __construct($templating)
    {
        $this->templating = $templating;
    }

    public function generateHtml($path)
    {
        try {
            $file = file_get_contents($path);
            $this->data = json_decode($file, true);
        } catch (\Exception $e) {
            throw new NotFoundHttpException("File is not exists");
        }

        try {
            $teams = $this->getTeamName();
            $score = $this->getScore($teams);
            $playersWithGoals = $this->getGoals($teams);
            $replacedPlayers = $this->getReplaced($teams);
            $cardsOfPlayers = $this->getCards($teams);
            $playersWithTime = $this->getTime($cardsOfPlayers, $replacedPlayers);
            $consist = $this->getPlayers($playersWithTime);

            $template = $this->templating->render('ProjectBundle:Default:parse.html.twig',
                [
                    'teamName' => $teams,
                    'score' => $score,
                    'info' => array_reverse($this->data),
                    'consist' => $consist,
                    'replacedPlayers' => $replacedPlayers,
                    'playersWithGoals' => $playersWithGoals,
                    'time' => $this->getTimeMatch()
                ]);

            $fileName = substr(md5($teams['team1'] . $teams['team2']), 0, 5) . '.html';
            $path = 'football-master/result/' . $fileName;

            file_put_contents($path, $template);

            return $path;

        } catch (\Exception $e) {
            throw new \Exception('Service is not available');
        }
    }

    public function getTeamName()
    {
        $info = $this->data;

        $teamName = [];
        foreach ($info as $value) {
            if ($value['type'] === 'startPeriod' && !empty($value['details'])) {
                $teamName['team1'] = $value['details']['team1']['title'];
                $teamName['team2'] = $value['details']['team2']['title'];
            }
        }

        return $teamName;
    }

    public function getScore($teams)
    {
        $goals = [
            'team1' => 0,
            'team2' => 0
        ];
        $info = $this->data;

        foreach ($info as $value) {
            if ($value['type'] === 'goal') {
                if ($value['details']['team'] === $teams['team1']) {
                    $goals['team1']++;
                } else {
                    $goals['team2']++;
                }
            }
        }

        return $goals;
    }

    public function getPlayers($players)
    {
        $info = $this->data;
        $activePlayers = [];
        $passivePlayers = [];

        foreach ($info as $value) {
            if ($value['type'] === 'startPeriod' && !empty($value['details']['team1'])) {

                foreach ($players['team1'] as $player) {
                    $status = true;
                    foreach ($value['details']['team1']['startPlayerNumbers'] as $number) {
                        if ($player['player']['number'] === $number) {
                            $activePlayers['team1'][] = $player;
                            $status = false;
                        }
                    }

                    if ($status) {
                        $passivePlayers['team1'][] = $player;
                    }
                }
            }

            if ($value['type'] === 'startPeriod' && !empty($value['details']['team2'])) {

                foreach ($players['team2'] as $player) {
                    $status = true;
                    foreach ($value['details']['team2']['startPlayerNumbers'] as $number) {
                        if ($player['player']['number'] === $number) {
                            $activePlayers['team2'][] = $player;
                            $status = false;
                        }
                    }

                    if ($status) {
                        $passivePlayers['team2'][] = $player;
                    }
                }
            }
        }

        return [
            'team1' => [
                'activePlayers' => $activePlayers['team1'],
                'passivePlayers' => $passivePlayers['team1']
            ],
            'team2' => [
                'activePlayers' => $activePlayers['team2'],
                'passivePlayers' => $passivePlayers['team2']
            ]
        ];
    }

    public function getReplaced($teams)
    {
        $info = $this->data;
        $playersList = $this->playersList($info);
        $replacedPlayers = [];

        foreach ($info as $value) {
            if ($value['type'] === 'replacePlayer') {
                $replacedPlayers[] = $value;
            }
        }

        $replaced = [];
        foreach ($replacedPlayers as $replacedPlayer) {
            if ($replacedPlayer['details']['team'] === $teams['team1']) {
                foreach ($playersList['team1'] as $player) {
                    if ($replacedPlayer['details']['inPlayerNumber'] === $player['number']) {
                        $replaced['team1']['inPlayers'][$player['number']] = [
                            'name' => $player['name'],
                            'time' => $replacedPlayer['time']
                        ];
                    }

                    if ($replacedPlayer['details']['outPlayerNumber'] === $player['number']) {
                        $replaced['team1']['outPlayers'][$player['number']] = [
                            'name' => $player['name'],
                            'time' => $replacedPlayer['time']
                        ];
                    }
                }
            }

            if ($replacedPlayer['details']['team'] === $teams['team2']) {
                foreach ($playersList['team2'] as $player) {
                    if ($replacedPlayer['details']['inPlayerNumber'] === $player['number']) {
                        $replaced['team2']['inPlayers'][$player['number']] = [
                            'name' => $player['name'],
                            'time' => $replacedPlayer['time']
                        ];
                    }

                    if ($replacedPlayer['details']['outPlayerNumber'] === $player['number']) {
                        $replaced['team2']['outPlayers'][$player['number']] = [
                            'name' => $player['name'],
                            'time' => $replacedPlayer['time']
                        ];
                    }
                }
            }
        }

        return $replaced;
    }

    public function playersList($info)
    {
        $team1 = [];
        $team2 = [];

        foreach ($info as $value) {
            if ($value['type'] === 'startPeriod' && !empty($value['details']['team1'])) {
                $team1 = $value['details']['team1']['players'];
            }

            if ($value['type'] === 'startPeriod' && !empty($value['details']['team2'])) {
                $team2 = $value['details']['team2']['players'];
            }
        }

        return [
            'team1' => $team1,
            'team2' => $team2
        ];
    }

    public function getTime($players, $replacedPlayers)
    {
        $finalTime = $this->getTimeMatch()['finalTime'];

        foreach ($replacedPlayers['team1']['outPlayers'] as $key => $value) {
            for ($i = 0; $i < count($players['team1']); $i++) {
                if ($players['team1'][$i]['player']['number'] === $key) {
                    $players['team1'][$i]['player']['time'] = $value['time'];
                }
            }
        }

        foreach ($replacedPlayers['team1']['inPlayers'] as $key => $value) {
            for ($i = 0; $i < count($players['team1']); $i++) {
                if ($players['team1'][$i]['player']['number'] === $key) {
                    $players['team1'][$i]['player']['time'] = $finalTime - $value['time'];
                }
            }
        }

        foreach ($replacedPlayers['team2']['outPlayers'] as $key => $value) {
            for ($i = 0; $i < count($players['team2']); $i++) {
                if ($players['team2'][$i]['player']['number'] === $key) {
                    $players['team2'][$i]['player']['time'] = $value['time'];
                }
            }
        }

        foreach ($replacedPlayers['team2']['inPlayers'] as $key => $value) {
            for ($i = 0; $i < count($players['team2']); $i++) {
                if ($players['team2'][$i]['player']['number'] === $key) {
                    $players['team2'][$i]['player']['time'] = $finalTime - $value['time'];
                }
            }
        }

        return $players;
    }

    public function getTimeMatch()
    {
        $info = $this->data;
        $startTime = 0;
        $finalTime = 0;

        foreach (array_reverse($info) as $value) {
            if ($value['type'] === 'finishPeriod') {
                $finalTime = $value['time'];
                break;
            }
        }

        return [
            'startTime' => $startTime,
            'finalTime' => $finalTime
        ];
    }

    public function getGoals($teams)
    {
        $info = $this->data;
        $players = $this->playersList($info);
        $goals = [];
        foreach ($info as $value) {
            if ($value['type'] === 'goal' && ($value['details']['team'] === $teams['team1'])) {
                $goals[] = ['number' => $value['details']['playerNumber'], 'team' => $value['details']['team']];
            }

            if ($value['type'] === 'goal' && ($value['details']['team'] === $teams['team2'])) {
                $goals[] = ['number' => $value['details']['playerNumber'], 'team' => $value['details']['team']];
            }
        }

        $playersWithGoal = [];
        foreach ($players['team1'] as $player) {
            foreach ($goals as $goal) {
                if ($goal['number'] === $player['number'] && ($goal['team'] === $teams['team1'])) {
                    $playersWithGoal['team1'][] = $player;
                }
            }
        }

        foreach ($players['team2'] as $player) {
            foreach ($goals as $goal) {
                if ($goal['number'] === $player['number'] && ($goal['team'] === $teams['team2'])) {
                    $playersWithGoal['team2'][] = $player;
                }
            }
        }

        $team1 = [];
        $team2 = [];
        foreach ($playersWithGoal['team1'] as $goal) {
            if (isset($team1[$goal['name']])) {
                $team1[$goal['name']]++;
            } else {
                $team1[$goal['name']] = 1;
            }
        }

        foreach ($playersWithGoal['team2'] as $goal) {
            if (isset($team2[$goal['name']])) {
                $team2[$goal['name']]++;
            } else {
                $team2[$goal['name']] = 1;
            }
        }

        return [
            'team1' => $team1,
            'team2' => $team2
        ];
    }

    public function getCards($teams)
    {
        $info = $this->data;
        $players = $this->playersList($info);

        $card = [];
        foreach ($info as $value) {
            if ($value['type'] === 'yellowCard') {
                if ($value['details']['team'] === $teams['team1']) {

                    if (isset($card['team1'][$value['details']['playerNumber']]['yellowCard'])) {
                        $count = $card['team1'][$value['details']['playerNumber']]['yellowCard']['count'];
                        $card['team1'][$value['details']['playerNumber']]['yellowCard'] = [
                            'type' => $value['type'],
                            'count' => ++$count
                        ];
                    } else {
                        $card['team1'][$value['details']['playerNumber']]['yellowCard'] = ['type' => $value['type'], 'count' => 1];
                    }
                }
                if ($value['details']['team'] === $teams['team2']) {
                    if (isset($card['team2'][$value['details']['playerNumber']]['yellowCard'])) {
                        $count = $card['team2'][$value['details']['playerNumber']]['yellowCard']['count'];
                        $card['team2'][$value['details']['playerNumber']]['yellowCard'] = [
                            'type' => $value['type'],
                            'count' => ++$count
                        ];
                    } else {
                        $card['team2'][$value['details']['playerNumber']]['yellowCard'] = ['type' => $value['type'], 'count' => 1];
                    }
                }
            }

            if ($value['type'] === 'redCard') {
                if ($value['details']['team'] === $teams['team1']) {

                    if (isset($card['team1'][$value['details']['playerNumber']]['redCard'])) {
                        $count = $card['team1'][$value['details']['playerNumber']]['redCard']['count'];
                        $card['team1'][$value['details']['playerNumber']]['redCard'] = [
                            'type' => $value['type'],
                            'count' => ++$count
                        ];
                    } else {
                        $card['team1'][$value['details']['playerNumber']]['redCard'] = ['type' => $value['type'], 'count' => 1];
                    }
                }
                if ($value['details']['team'] === $teams['team2']) {
                    if (isset($card['team2'][$value['details']['playerNumber']]['redCard'])) {
                        $count = $card['team1'][$value['details']['playerNumber']]['redCard']['count'];
                        $card['team2'][$value['details']['playerNumber']]['redCard'] = [
                            'type' => $value['type'],
                            'count' => ++$count
                        ];
                    } else {
                        $card['team2'][$value['details']['playerNumber']]['redCard'] = ['type' => $value['type'], 'count' => 1];
                    }
                }
            }
        }

        $playersWithCard = [];
        foreach ($players['team1'] as $player) {
            $status = true;
            foreach ($card['team1'] as $key => $value) {
                if ($key === $player['number']) {
                    $playersWithCard['team1'][] = ['player' => $player, 'card' => $value];
                    $status = false;
                }
            }
            if ($status) {
                $playersWithCard['team1'][] = ['player' => $player];
            }
        }

        foreach ($players['team2'] as $player) {
            $status = true;
            foreach ($card['team2'] as $key => $value) {
                if ($key === $player['number']) {
                    $playersWithCard['team2'][] = ['player' => $player, 'card' => $value];
                    $status = false;
                }
            }
            if ($status) {
                $playersWithCard['team2'][] = ['player' => $player];
            }
        }

        return $playersWithCard;
    }
}