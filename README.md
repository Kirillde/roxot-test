Roxot test task
===========
Kirill Kuchirov

примерно 15 часов

**_Структура:_**

ProjectBundle:

DefaultController - принимает параметры, передаёт данные в модель и т.д.

Model:
Processing - генерирует html

View:
Resource/views - шаблоны

**_Установка:_**

git clone

download composer

install composer

install bower

Run url: web/app.php (or app_dev.php)

json файл хранится в web/football-master/source

результат - web/football-master/result
