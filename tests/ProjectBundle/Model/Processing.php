<?php

namespace Tests\AppBundle\Model;

use ProjectBundle\Model\Processing;

class ProcessingTest extends \PHPUnit_Framework_TestCase
{
    public function testAdd()
    {
        $calc = new Processing();
        $result = $calc->add(30, 12);


        $this->assertEquals(42, $result);
    }
}